Zuul-client - User CLI for the Zuul Project Gating System
=========================================================

zuul-client is a simple command line client that may be used to query Zuul's
state or affect its behavior, granted the user is allowed to do so. It must be
run on a host with access to Zuul's web server.

Documentation
-------------

.. toctree::
   :maxdepth: 2

   installation
   configuration
   commands
   releasenotes
